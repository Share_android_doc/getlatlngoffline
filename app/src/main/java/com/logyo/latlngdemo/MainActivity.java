package com.logyo.latlngdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String latitude, longitude;
    public static final int REQUEST_LOCATION = 1;
    TextView textView1, textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        textView1 = findViewById(R.id.tvLat);
        textView2 = findViewById(R.id.tvLng);

    }


    public void onGetLatLng(View view) {

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location==null){
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Log.e("data", "data=>"+location.getLatitude());
            latitude= String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
        }

        textView2.setText(longitude);
        textView1.setText(latitude);
    }
}
